(function($) {

  termAutosuggest = function (val) {
    var element = $('#' + Drupal.settings.termAutosuggest.id);
    element.val(element.val() + ', ' + val);
  };

})(jQuery);
